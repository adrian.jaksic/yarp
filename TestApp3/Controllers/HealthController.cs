using Microsoft.AspNetCore.Mvc;

namespace TestApp3.Controllers
{
    [ApiController]
    [Route("api/healthz")]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public IDictionary<string, string> HealthCheck()
        {
            return new Dictionary<string, string>
            {
                { "Health", "Ok" }
            };
        }
    }
}