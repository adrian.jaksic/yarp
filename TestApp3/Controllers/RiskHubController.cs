using Microsoft.AspNetCore.Mvc;

namespace TestApp2.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RiskHubController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "RiskHub-3"
        };

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Summaries;
        }

        [HttpGet]
        [Route("something")]
        public string GetSomething(string param = null)
        {
            return "RiskHub-3 OK param: " + param;
        }
    }
}