using Microsoft.AspNetCore.Mvc;

namespace TestApp1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RiskHubV2Controller : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "RiskHubV2-3"
        };

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Summaries;
        }

        [HttpGet]
        [Route("something")]
        public string GetSomething(string param = null)
        {
            return "RiskHubV2-3 OK param: " + param;
        }
    }
}