using Yarp.MyReverseProxy.LoadBalancingPolicy;
using Yarp.MyReverseProxy.ProxyConfig;
using Yarp.ReverseProxy.Configuration;
using Yarp.ReverseProxy.Health;
using Yarp.ReverseProxy.LoadBalancing;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Register it in DI in ConfigureServices method
builder.Services.AddSingleton<ILoadBalancingPolicy, WeightedRoundRobinLoadBalancingPolicy>();

builder.Services.Configure<TransportFailureRateHealthPolicyOptions>(o =>
{
    o.DetectionWindowSize = TimeSpan.FromSeconds(30);
    o.MinimalTotalCountThreshold = 3;
    o.DefaultFailureRateLimit = 0.5;
});

builder.Services.AddSingleton<IProxyConfigProvider, YarpProxyConfigProvider>();
builder.Services.AddReverseProxy();

var app = builder.Build();

//Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseHttpLogging();
app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(
    endpoints =>
    {
        endpoints.MapControllers();
        app.MapReverseProxy(opt =>
        {
            opt.UseLoadBalancing();
            opt.UsePassiveHealthChecks();
            //opt.UseSessionAffinity();
        });
    });

app.Run();
