using Microsoft.AspNetCore.Mvc;
using Yarp.MyReverseProxy.Services;
using Yarp.MyReverseProxy.ServiceType;

namespace Yarp.MyReverseProxy.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RegisteredServicesController : ControllerBase
    {
        [HttpGet]
        [Route("types")]
        public IEnumerable<ServiceTypeDto> GetTypes()
        {
            return ServiceTypeProvider.GetTypes();
        }

        [HttpGet]
        public IEnumerable<ServiceInstanceDto> GetServices()
        {
            return ServiceInstanceProvider.GetServices();
        }

        [HttpPost]
        [Route("add")]
        public void AddServices([FromBody] ServiceInstanceDto add)
        {
            ServiceInstanceProvider.AddService(add);
        }

        [HttpPost]
        [Route("remove")]
        public void AddServices([FromQuery] string serviceName)
        {
            ServiceInstanceProvider.RemoveService(serviceName);
        }

        [HttpPatch]
        [Route("drain")]
        public void SetDrain([FromQuery] string serviceName, [FromQuery] bool drain)
        {
            ServiceInstanceProvider.SetServiceToDrain(serviceName, drain);
        }
    }
}