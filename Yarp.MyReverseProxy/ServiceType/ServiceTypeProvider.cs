﻿namespace Yarp.MyReverseProxy.ServiceType
{
    public static class ServiceTypeProvider
    {
        private static Dictionary<string, ServiceTypeDto> _types;
        static ServiceTypeProvider()
        {
            const string riskHubServiceType = "RiskHub";
            const string creditCheckServiceType = "CreditCheck";

            _types = new Dictionary<string, ServiceTypeDto>
            {
                { riskHubServiceType, new ServiceTypeDto { TypeName = riskHubServiceType, Path = "/RiskHub" } },
                { creditCheckServiceType, new ServiceTypeDto { TypeName = creditCheckServiceType, Path = "/CreditCheck" } }
            };
        }
        public static IEnumerable<ServiceTypeDto> GetTypes()
        {
            return _types.Values;
        }

        public static bool IsExistingType(string typeName)
        {
            return _types.ContainsKey(typeName);
        }

        public static ServiceTypeDto GetType(string typeName)
        {
            return _types.TryGetValue(typeName, out var type) ? type : null;
        }
    }
}
