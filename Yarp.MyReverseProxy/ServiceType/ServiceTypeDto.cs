﻿using System.Net.Mime;

namespace Yarp.MyReverseProxy.ServiceType
{
    public class ServiceTypeDto
    {
        public string TypeName { get; set; }
        public string Path { get; set; }
    }
}
