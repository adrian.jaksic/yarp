﻿namespace Yarp.MyReverseProxy.LoadBalancingPolicy
{
    public class ClusterWeightData
    {
        public int DestinationIndex { get; set; }
        public int CurrentWeight { get; set; }
        public string Version { get; set; }
    }
}
