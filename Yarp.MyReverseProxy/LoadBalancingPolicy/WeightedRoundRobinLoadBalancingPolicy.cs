﻿using Yarp.ReverseProxy.LoadBalancing;
using Yarp.ReverseProxy.Model;

namespace Yarp.MyReverseProxy.LoadBalancingPolicy
{
    public class WeightedRoundRobinLoadBalancingPolicy : ILoadBalancingPolicy
    {
        public string Name => "WeightedRoundRobin";

        public Dictionary<string, ClusterWeightData> _tracking = new Dictionary<string, ClusterWeightData>();

        public DestinationState? PickDestination(HttpContext context, ClusterState cluster, IReadOnlyList<DestinationState> availableDestinations)
        {
            var clusterVersion = cluster.Model.Config.Metadata["Version"];
            ClusterWeightData data;
            if (!(_tracking.TryGetValue(cluster.ClusterId, out data) && data.Version == clusterVersion))
            {
                data = new ClusterWeightData();
                data.Version = clusterVersion;
                _tracking.Add(cluster.ClusterId, data);
            }

            if (data.DestinationIndex >= availableDestinations.Count)
            {
                data.DestinationIndex = 0;
                data.CurrentWeight = 0;
            }

            if (availableDestinations[data.DestinationIndex].Model.Config.Metadata.TryGetValue("Weight", out var weightString) && int.TryParse(weightString, out int weight) && weight > 0)
            {
                data.CurrentWeight += 1;

                if (data.CurrentWeight >= weight)
                {
                    data.DestinationIndex += 1;
                    data.CurrentWeight = 0;
                }
            }
            else
            {
                data.DestinationIndex += 1;
                data.CurrentWeight = 0;
            }

            if (data.DestinationIndex >= availableDestinations.Count)
            {
                data.DestinationIndex = 0;
                data.CurrentWeight = 0;
            }

            return availableDestinations[data.DestinationIndex];
        }
    }
}
