﻿using Yarp.MyReverseProxy.ProxyConfig;

namespace Yarp.MyReverseProxy.Services
{
    public static class ServiceInstanceProvider
    {
        private static List<ServiceInstanceDto> _services;
        static ServiceInstanceProvider()
        {
            const string riskHubServiceType = "RiskHub";
            const string creditCheckServiceType = "CreditCheck";

            _services = new List<ServiceInstanceDto>
            {
                new ServiceInstanceDto { ServiceName = "RiskHubService1", ServiceTypeName = riskHubServiceType, ServiceUri = "https://localhost:7065/api/RiskHub", Weight = 2 },
                new ServiceInstanceDto { ServiceName = "RiskHubService2", ServiceTypeName = riskHubServiceType, ServiceUri = "https://localhost:7159/api/RiskHub", Weight = 1 },
                new ServiceInstanceDto { ServiceName = "CreditCheck1", ServiceTypeName = creditCheckServiceType, ServiceUri = "https://localhost:7190/api/CreditCheck", Weight = 1 }
            };
        }
        public static IEnumerable<ServiceInstanceDto> GetServices()
        {
            return _services;
        }

        public static void AddService(ServiceInstanceDto service)
        {
            if (!_services.Any(x => x.ServiceName == service.ServiceName))
            {
                _services.Add(service);
                InMemoryYarpProxyConfig.RefreshClusters();
            }
        }

        public static void RemoveService(string serviceName)
        {
            var foundService = _services.FirstOrDefault(x => x.ServiceName == serviceName);
            if (foundService != null)
            {
                _services.Remove(foundService);
                InMemoryYarpProxyConfig.RefreshClusters();
            }
        }

        public static void SetServiceToDrain(string serviceName, bool drain)
        {
            var foundService = _services.FirstOrDefault(x => x.ServiceName == serviceName);
            if (foundService != null)
            {
                foundService.Draining = drain;
                InMemoryYarpProxyConfig.RefreshClusters();
            }
        }
    }
}
