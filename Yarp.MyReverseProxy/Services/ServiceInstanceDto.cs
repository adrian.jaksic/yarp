﻿namespace Yarp.MyReverseProxy.Services
{
    public class ServiceInstanceDto
    {
        public string ServiceName { get; set; }
        public string ServiceTypeName { get; set; }
        public string ServiceUri { get; set; }
        public bool Draining { get; set; }
        public int Weight { get; set; }
    }
}
