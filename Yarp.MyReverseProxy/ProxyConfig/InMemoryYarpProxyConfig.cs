﻿using Microsoft.Extensions.Primitives;
using Yarp.MyReverseProxy.Services;
using Yarp.MyReverseProxy.ServiceType;
using Yarp.ReverseProxy.Configuration;
using Yarp.ReverseProxy.Transforms;

namespace Yarp.MyReverseProxy.ProxyConfig
{
    public class InMemoryYarpProxyConfig : IProxyConfig
    {
        private static List<RouteConfig> _routes;
        private static List<ClusterConfig> _clusters;
        private static CancellationChangeToken _changeToken;
        private static CancellationTokenSource _cts = new CancellationTokenSource();
        private static int _version = 0;

        static InMemoryYarpProxyConfig()
        {
            GenerateRoutes();
            GenerateClusters();
        }

        public InMemoryYarpProxyConfig()
        {
            _cts = new CancellationTokenSource();
            _changeToken = new CancellationChangeToken(_cts.Token);
            GenerateRoutes();
            GenerateClusters();
        }

        public IReadOnlyList<RouteConfig> Routes => _routes;
        public IReadOnlyList<ClusterConfig> Clusters => _clusters;
        public IChangeToken ChangeToken => _changeToken;

        public static void RefreshClusters()
        {
            GenerateClusters();
            _cts.Cancel();
        }

        private static void GenerateClusters()
        {
            _version++;
            var services = ServiceInstanceProvider.GetServices();

            var clusters = new List<ClusterConfig>();
            foreach (var groupedServices in services.Where(x => !x.Draining).GroupBy(x => x.ServiceTypeName))
            {
                var destinations = new Dictionary<string, DestinationConfig>();
                foreach (var destination in groupedServices)
                {
                    var destinationConfig = new DestinationConfig
                    {
                        Address = destination.ServiceUri,
                        Metadata = new Dictionary<string, string> { { "Weight", destination.Weight.ToString() } },
                    };
                    destinations.Add(destination.ServiceName, destinationConfig);
                }

                var cluster = new ClusterConfig()
                {
                    ClusterId = groupedServices.Key,
                    LoadBalancingPolicy = "WeightedRoundRobin", // RoundRobin, WeightedRoundRobin
                    Destinations = destinations,
                    Metadata = new Dictionary<string, string> { { "Version", _version.ToString() } },
                };

                clusters.Add(cluster);
            }

            _clusters = clusters;
        }

        private static void GenerateRoutes()
        {
            var collection = new List<RouteConfig>();

            var serviceTypes = ServiceTypeProvider.GetTypes();

            foreach (var serviceType in serviceTypes)
            {
                var routeConfig = new RouteConfig()
                {
                    RouteId = serviceType.TypeName,
                    ClusterId = serviceType.TypeName,
                    Match = new RouteMatch()
                    {
                        Path = serviceType.Path + "/{**catch-all}",
                    },
                    Transforms = new List<Dictionary<string, string>>(),
                };

                routeConfig = routeConfig.WithTransformPathRemovePrefix(new PathString(serviceType.Path));
                
                collection.Add(routeConfig);
            }

            _routes = collection;
        }
    }
}
