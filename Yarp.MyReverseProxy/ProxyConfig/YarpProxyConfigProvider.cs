﻿using Yarp.ReverseProxy.Configuration;

namespace Yarp.MyReverseProxy.ProxyConfig
{
    public class YarpProxyConfigProvider : IProxyConfigProvider
    {
        public IProxyConfig GetConfig()
        {
            return new InMemoryYarpProxyConfig();
        }
    }
}
