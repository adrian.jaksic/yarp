using Microsoft.AspNetCore.Mvc;

namespace TestApp3.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CreditCheckController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "CreditCheck-2"
        };

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Summaries;
        }

        [HttpGet]
        [Route("something")]
        public string GetSomething(string param = null)
        {
            return "CreditCheck-2 param: " + param;
        }
    }
}