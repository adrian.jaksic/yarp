using Microsoft.AspNetCore.Mvc;

namespace TestApp2.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RiskHubV2Controller : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "RiskHubV2-2"
        };

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Summaries;
        }

        [HttpGet]
        [Route("something")]
        public string GetSomething(string param = null)
        {
            return "RiskHubV2-2 OK param: " + param;
        }
    }
}