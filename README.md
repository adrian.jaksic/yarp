# YARP
YARP is designed as a library that provides the core proxy functionality which you can then customize by adding or replacing modules.\

## Solution organisation
3 API projects (TestApp1, TestApp2, TestApp3). \
-Each project has 3 controllers RiskHub, RiskHubV2, CreditCheck + Health \
-Each controller (except Health) have / and /something?param=value endpoints for testing \
Yarp.MyReverseProxy is the main app that will do the action!

## Sources used
https://microsoft.github.io/reverse-proxy/ \
https://www.codemag.com/Article/2209031/YARP-I-Did-It-Again

## Logging
YARP diagnosing \
request logging (ASP .NET 6)

## Websocket and Grpc
YARP enables proxying WebSocket and SPDY connections by default. This support works with both the direct forwarding and full pipeline approaches. \
GRPC: protocol need to be configured https://microsoft.github.io/reverse-proxy/articles/grpc.html

## AB Testing and Rolling update
AB Testing: easiest with a specific header??? require manual work on YARP. \
Rolling update: working

## Session Affinity
https://microsoft.github.io/reverse-proxy/articles/session-affinity.html

## Distributed tracing
https://microsoft.github.io/reverse-proxy/articles/distributed-tracing.html

## Health check
-Pasive and active healthcheck (we should use both)

## Usage
-can be used ass one global proxy or as separate proxy for group of services (we can start as one?)

## Guideline for http client usage
-With polly (retry) \
-Headers that need to be copied from origin request \
-Distributed tracing

## Additional
-Service dashboard: For healthcheck (show status of all services) and service dashboard (can update drain, add service, remove service) \
-Set up Kibana or some other tool for log search

## Testing
Start all 4 projects.

Requests for testing reverse proxy:\
https://localhost:7128/riskhub \
https://localhost:7128/riskhub/something?param=test \
https://localhost:7128/creditcheck \
https://localhost:7128/creditcheck/something?param=test \
\
Through swagger add/remove services.
\
Adding service instances (body):\
{
  "serviceName": "RiskHubService3",
  "serviceTypeName": "RiskHub",
  "serviceUri": "https://localhost:7190/api/RiskHub",
  "draining": false
}
\
\
V2 service:\
{
  "serviceName": "RiskHubService3_V2",
  "serviceTypeName": "RiskHub",
  "serviceUri": "https://localhost:7190/api/RiskHubV2",
  "draining": false
}
\
\
Not responding service:\
{
  "serviceName": "RiskHubService4_NotResponding",
  "serviceTypeName": "RiskHub",
  "serviceUri": "https://localhost:7199/api/RiskHub",
  "draining": false
}
