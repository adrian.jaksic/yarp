using Microsoft.AspNetCore.Mvc;

namespace TestApp1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RiskHubController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "RiskHub-1"
        };

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Summaries;
        }

        [HttpGet]
        [Route("something")]
        public string GetSomething(string param = null)
        {
            return "RiskHub-1 OK param: " + param;
        }
    }
}