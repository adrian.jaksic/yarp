using Microsoft.AspNetCore.Mvc;

namespace TestApp1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CreditCheckController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "CreditCheck-1"
        };

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return Summaries;
        }

        [HttpGet]
        [Route("something")]
        public string GetSomething(string param = null)
        {
            return "CreditCheck-1 param: " + param;
        }
    }
}